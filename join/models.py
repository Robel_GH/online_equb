from django.db import models
from django.conf import settings


class Periods(models.Model):
    period = models.CharField(max_length=255, unique=True, blank=True, null=True)

    def __str__(self):
        return self.period


class MonthDurations(models.Model):
    duration = models.IntegerField(unique=True, blank=True, null=True)

    def __str__(self):
        return str(self.duration)


class Amounts(models.Model):
    amount = models.IntegerField(unique=True, blank=True, null=True)

    def __str__(self):
        return str(self.amount)


class MonthCategory(models.Model):
    cat_name = models.CharField(max_length=255, unique=True, blank=True, null=True)
    cat_period = models.ForeignKey(Periods, related_name="periods", on_delete=models.CASCADE, null=True, blank=True)
    cat_duration = models.ForeignKey(MonthDurations, related_name="durations", on_delete=models.CASCADE, null=True,
                                     blank=True)
    cat_amount = models.ForeignKey(Amounts, related_name="amounts", on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.cat_name


class Status(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="statuses", on_delete=models.CASCADE, blank=True, null=True)
    category = models.ForeignKey(MonthCategory, related_name="categories", on_delete=models.CASCADE, blank=True,
                                 null=True)
    is_active = models.BooleanField(default=False)
    is_pending = models.BooleanField(default=True)

    def __str__(self):
        return self.owner


class Activities(models.Model):
    category = models.ForeignKey(MonthCategory, related_name="activities_categories", on_delete=models.CASCADE,
                                 blank=True,
                                 null=True)
    counter = models.IntegerField(default=0)
    group_counter = models.IntegerField(default=0)

    def __str__(self):
        return MonthCategory.objects.get()


class Group(models.Model):
    group_name = models.CharField(max_length=255, blank=True, null=True)
    member = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="members", through="MembersData",
                                    blank=True)

    def __str__(self):
        return self.group_name


class MembersData(models.Model):
    group = models.ForeignKey(Group, related_name="groups", null=True, blank=True, on_delete=models.CASCADE)
    member = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="member_data", on_delete=models.CASCADE, blank=True, null=True)
    category = models.ForeignKey(MonthCategory, related_name="member_cat", on_delete=models.CASCADE, blank=True,
                                 null=True)
    season = models.IntegerField(default=0)

    def __str__(self):
        return self.member
