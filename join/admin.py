from django.contrib import admin
from . import models

admin.site.register(models.MonthCategory)
admin.site.register(models.Group)
admin.site.register(models.Amounts)
admin.site.register(models.MonthDurations)
admin.site.register(models.Periods)
admin.site.register(models.Activities)
admin.site.register(models.MembersData)
admin.site.register(models.Status)
