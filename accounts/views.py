from rest_framework.views import APIView
from rest_framework.response import Response
from .models import PhoneOtp, User
from rest_framework import status, permissions, generics, viewsets
from django.shortcuts import get_object_or_404
# from .serializers import UserSerializer, LoginSerializer, RegisterUserSerializer, ProfileSerializer
from .serializers import ProfileSerializer, LoginSerializer, UserSerializer
from knox.views import LoginView as KnoxLoginView
from knox.models import AuthToken
# from knox.auth import TokenAuthentication
from rest_framework import filters
from django.contrib.auth import login
import random
import requests


class ValidateUser(APIView):
    def post(self, request, *args, **kwargs):

        phone_sent = request.data.get("phone_number", False)
        if phone_sent:
            phone_temp1 = str(phone_sent)
            phone_temp2 = PhoneOtp.objects.filter(phone_number__iexact=phone_temp1)
            if phone_temp2.exists():
                return Response({
                    'status': False,
                    'detail': 'Phone number already exist',
                })
            else:
                key = otp_generator(phone_temp1)
                str_key = str(key)
                PhoneOtp.objects.create(
                    phone_number=phone_temp1,
                    otp=str_key,
                    count=1
                )

                if key:
                    # requests.request("GET", "https://2factor.in/API/V1/d674a078-7b91-11e9-ade6-0200cd936042/SMS/"+phone_temp1+"/"+str_key)

                    return Response({
                        'status': True,
                        'detail': key
                    })
                else:
                    return Response({
                        'status': False,
                        'detail': 'error in generating otp'
                    })
        else:
            return Response({
                'status': False,
                'detail': 'Phone number is not received from the form'
            })


def otp_generator(phone):
    if phone:
        key = random.randint(99999, 999999)
        return key
    else:
        return False


class ValidateOtp(APIView):

    def post(self, request, *args, **kwargs):

        phone_sent = request.data.get('phone_number', False)
        otp_sent = request.data.get('otp', False)

        if phone_sent and otp_sent:
            phone_temp1 = str(phone_sent)
            obj1 = PhoneOtp.objects.filter(phone_number__iexact=phone_temp1)

            if obj1.exists():
                obj2 = obj1.first()

                if str(obj2.otp) == str(otp_sent):
                    obj2.validated = True
                    obj2.save()
                    return Response({
                        'status': True,
                        'detail': 'Otp received successfully!'
                    })

                else:
                    return Response({
                        'status': False,
                        'detail': 'INCORRECT OTP!'
                    })

            else:
                return Response({
                    'status': False,
                    'detail': 'Phone number does not exist'
                })

        return Response({
            'status': False,
            'detail': 'Phone or otp does not received!'
        })


class RegisterUserAPIViewSet(viewsets.ModelViewSet):
    """ The functionallity of this class allows the user to register
     themeselves as a regular based user in our system """

    queryset = User.objects.all()
    serializer_class = ProfileSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name', 'email', 'phone_number')

    def create(self, request):
        """ This validates and saves the registered regular user
         in the database. """

        serializer = ProfileSerializer(data=request.data)
        queryset = User.objects.all()

        if serializer.is_valid():
            user = serializer.save()
            """ids = serializer.data.get('id')
            first_name = serializer.data.get('first_name')
            last_name = serializer.data.get('last_name')
            message = "Hellow ID:{}, {} {}".format(ids, first_name, last_name)

            return Response({'message': message})
            user = serializer.save()"""
            return Response({
                "user": UserSerializer(user, context=self.get_serializer_context()).data,
                "token": AuthToken.objects.create(user)[1]
            })
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class LoginApi(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        # pwd = User.objects.filter(password__iexact=str(request.data.get('password')))
        serializer = LoginSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super().post(request, format=None)
