from rest_framework import serializers
from .models import User
from django.contrib.auth import authenticate


class ProfileSerializer(serializers.ModelSerializer):
    """ A serializer for our user profiles """

    id = serializers.ReadOnlyField()
    is_active = serializers.ReadOnlyField()
    password = serializers.CharField(max_length=255,
                                     style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ['id', 'is_active', 'first_name',
                  'last_name', 'email', 'password', 'phone_number']

        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data, **extra_fields):
        """ Creates and returns a new user """

        # Validating Data
        user = User.objects.create_user(phone_number=validated_data['phone_number'],
                                        password=validated_data['password'],
                                        email=validated_data['email'],
                                        first_name=validated_data['first_name'],
                                        last_name=validated_data['last_name'],
                                        )
        user.save()

        return user


class LoginSerializer(serializers.Serializer):
    phone_number = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'}, trim_whitespace=False)

    def authenticate(self, **kwargs):
        return authenticate(self.context['request'], **kwargs)

    def validate(self, data):
        phone_number = data.get('phone_number')
        password = data.get('password')

        if phone_number and password:
            obj1 = User.objects.filter(phone_number=phone_number)
            if obj1.exists():

                # user = authenticate(phone_number=phone_number, password=password)

                user = self.authenticate(phone_number=phone_number, password=password)
            # user = authenticate(request=self.context.get("request"), phone_number=phone, password=password)

            else:
                msg = {
                    'status': False,
                    'detail': 'Phone number does not exist'
                }
                raise serializers.ValidationError(msg, code='authorization')
            if not user:
                msg = {
                    'status': False,
                    'detail': 'Phone number and password does not match, try again!'
                }
                raise serializers.ValidationError(msg, code='authorization')

        else:
            msg = {
                'status': False,
                'detail': 'Phone or password don not received',
            }
            raise serializers.ValidationError(msg, code='authorization')
        data['user'] = user
        return data


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
